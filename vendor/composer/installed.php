<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '8035e1e75b4efe849fa76ed5e1ebceb590adb909',
        'name' => 'topthink/think',
        'dev' => true,
    ),
    'versions' => array(
        'adbario/php-dot-notation' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../adbario/php-dot-notation',
            'aliases' => array(),
            'reference' => 'eee4fc81296531e6aafba4c2bbccfc5adab1676e',
            'dev_requirement' => false,
        ),
        'alibabacloud/client' => array(
            'pretty_version' => '1.5.31',
            'version' => '1.5.31.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/client',
            'aliases' => array(),
            'reference' => '19224d92fe27ab8ef501d77d4891e7660bc023c1',
            'dev_requirement' => false,
        ),
        'clagiordano/weblibs-configmanager' => array(
            'pretty_version' => 'v1.5.0',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clagiordano/weblibs-configmanager',
            'aliases' => array(),
            'reference' => '8802c7396d61a923c9a73e37ead062b24bb1b273',
            'dev_requirement' => false,
        ),
        'danielstjules/stringy' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../danielstjules/stringy',
            'aliases' => array(),
            'reference' => 'df24ab62d2d8213bbbe88cc36fc35a4503b4bd7e',
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.13.0',
            'version' => '4.13.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.4.0',
            'version' => '7.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => '868b3571a039f0ebc11ac8f344f4080babe2cb94',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => '089edd38f5b8abba6cb01567c2a8aaa47cec4c72',
            'dev_requirement' => false,
        ),
        'lcobucci/jwt' => array(
            'pretty_version' => '3.3.3',
            'version' => '3.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lcobucci/jwt',
            'aliases' => array(),
            'reference' => 'c1123697f6a2ec29162b82f170dd4a491f524773',
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.8',
            'version' => '1.1.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'reference' => 'c995bb0c23c58c9813d081f9523c9b7bb496698e',
            'dev_requirement' => false,
        ),
        'league/flysystem-cached-adapter' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem-cached-adapter',
            'aliases' => array(),
            'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.9.0',
            'version' => '1.9.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'reference' => 'aa70e813a6ad3d1558fc927863d47309b4c23e69',
            'dev_requirement' => false,
        ),
        'maennchen/zipstream-php' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maennchen/zipstream-php',
            'aliases' => array(),
            'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
            'dev_requirement' => false,
        ),
        'markbaker/complex' => array(
            'pretty_version' => '3.0.1',
            'version' => '3.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/complex',
            'aliases' => array(),
            'reference' => 'ab8bc271e404909db09ff2d5ffa1e538085c0f22',
            'dev_requirement' => false,
        ),
        'markbaker/matrix' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/matrix',
            'aliases' => array(),
            'reference' => 'c66aefcafb4f6c269510e9ac46b82619a904c576',
            'dev_requirement' => false,
        ),
        'mtdowling/jmespath.php' => array(
            'pretty_version' => '2.6.1',
            'version' => '2.6.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mtdowling/jmespath.php',
            'aliases' => array(),
            'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
            'dev_requirement' => false,
        ),
        'myclabs/php-enum' => array(
            'pretty_version' => '1.8.3',
            'version' => '1.8.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/php-enum',
            'aliases' => array(),
            'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.5.3',
            'version' => '6.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'reference' => 'baeb7cde6b60b1286912690ab0693c7789a31e71',
            'dev_requirement' => false,
        ),
        'phpoffice/phpspreadsheet' => array(
            'pretty_version' => '1.20.0',
            'version' => '1.20.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpspreadsheet',
            'aliases' => array(),
            'reference' => '44436f270bb134b4a94670f3d020a85dfa0a3c02',
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => true,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.34',
            'version' => '4.4.34.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'reference' => '2d0c056b2faaa3d785bdbd5adecc593a5be9c16e',
            'dev_requirement' => true,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'reference' => '02c1859112aa779d9ab394ae4f3381911d84052b',
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v6.0.9',
            'version' => '6.0.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/framework',
            'aliases' => array(),
            'reference' => '0b5fb453f0e533de3af3a1ab6a202510b61be617',
            'dev_requirement' => false,
        ),
        'topthink/think' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '8035e1e75b4efe849fa76ed5e1ebceb590adb909',
            'dev_requirement' => false,
        ),
        'topthink/think-captcha' => array(
            'pretty_version' => 'v3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-captcha',
            'aliases' => array(),
            'reference' => '1eef3717c1bcf4f5bbe2d1a1c704011d330a8b55',
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'reference' => 'f98e3ad44acd27ae85a4d923b1bdfd16c6d8d905',
            'dev_requirement' => false,
        ),
        'topthink/think-multi-app' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-multi-app',
            'aliases' => array(),
            'reference' => 'ccaad7c2d33f42cb1cc2a78d6610aaec02cea4c3',
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.45',
            'version' => '2.0.45.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'reference' => '3dcf9af447b048103093840833e8c74ab849152f',
            'dev_requirement' => false,
        ),
        'topthink/think-template' => array(
            'pretty_version' => 'v2.0.8',
            'version' => '2.0.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-template',
            'aliases' => array(),
            'reference' => 'abfc293f74f9ef5127b5c416310a01fe42e59368',
            'dev_requirement' => false,
        ),
        'topthink/think-trace' => array(
            'pretty_version' => 'v1.4',
            'version' => '1.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-trace',
            'aliases' => array(),
            'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
            'dev_requirement' => true,
        ),
        'topthink/think-view' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-view',
            'aliases' => array(),
            'reference' => 'edce0ae2c9551ab65f9e94a222604b0dead3576d',
            'dev_requirement' => false,
        ),
        'tp5er/tp5-databackup' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tp5er/tp5-databackup',
            'aliases' => array(),
            'reference' => 'f492b6c1faf01dd97e4ed689eb47c8b822bb50aa',
            'dev_requirement' => false,
        ),
        'zzstudio/think-addons' => array(
            'pretty_version' => '2.0.5',
            'version' => '2.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../zzstudio/think-addons',
            'aliases' => array(),
            'reference' => '7eb740cb219a111d593a05ad88248a74f640fe5c',
            'dev_requirement' => false,
        ),
    ),
);
